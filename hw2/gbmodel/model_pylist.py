"""
Python list model
"""
from datetime import date
from .Model import Model

class model(Model):
    def __init__(self):
        self.guestentries = []

    def select(self):
        """
        Returns guestentries list of lists
        Each list in guestentries contains: department, course number, quarter, year, instructor, review
        :return: List of lists
        """
        return self.guestentries

    def insert(self, department, course_num, quarter, year, instructor, review):
        """
        Appends a new list of values representing new message into guestentries
        :param department: String
        :param course_num: String
        :param quarter: String
        :param year: Int
        :param instructor: String
        :param review: String
        :return: True
        """
        params = [department, course_num, quarter, year, instructor, review]
        self.guestentries.append(params)
        return True
