from flask import redirect, request, url_for, render_template
from flask.views import MethodView
import query
import image

class Index(MethodView):
    def get(self):
        return render_template('index.html')

    def post(self):
        """
        Accepts POST requests, and processes the form;
        Renders index with the results when completed.
        """
        college_name = request.form['college']
        gen_info_results = query.gen_info(college_name)
        past_record_results = query.past_records(college_name)

        if (past_record_results.empty):
            # query did not find a match
            return render_template('index.html', error='true')
        else:
            # creates and uploads record image to storage bucket    
            image.create_image(college_name, past_record_results)

            # renders the index.html with the query results and graph
            return render_template('index.html', results=gen_info_results, record_image=image.retrieve_image(college_name))
