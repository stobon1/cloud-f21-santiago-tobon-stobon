from google.cloud import bigquery
import matplotlib.pyplot as plt

# queries bigquery public ncaa basketball dataset and gets some general info about a particular college
def gen_info(college_name):
    query_string = """
    SELECT T.market, T.name, T.turner_name, T.venue_city, T.venue_state, T.venue_name, T.venue_capacity, T.conf_alias, M.mascot, M.mascot_name
    FROM bigquery-public-data.ncaa_basketball.mbb_teams as T JOIN bigquery-public-data.ncaa_basketball.mascots as M
    ON T.market = M.market
    WHERE T.market = """ + "'{}'".format(college_name)

    query_result = bigquery.Client().query(query_string)
    return query_result

# queries bigquery public ncaa basketball dataset and gets their historical season records
def past_records(college_name):
    query_string = """
    SELECT S.season, S.wins, S.losses
    FROM bigquery-public-data.ncaa_basketball.mbb_teams as T JOIN bigquery-public-data.ncaa_basketball.mbb_historical_teams_seasons as S
    ON T.code_ncaa = S.team_code
    WHERE T.market = """ + "'{}'".format(college_name) + """ AND S.season > 1959
    ORDER BY S.season """    

    query_result = bigquery.Client().query(query_string).to_dataframe()
    return query_result
