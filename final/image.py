from google.cloud import storage
import matplotlib.pyplot as plt

# takes the query result as a dataframe and plots it and saves it as a image
def create_image(college_name, query_result):
    filename = f'images/{college_name}-past-record.jpg'
    plot = query_result.plot(x='season', y='wins', kind='bar', figsize=(10, 6))
    fig = plot.get_figure()
    fig.savefig(filename)
    upload_image(filename)
    return

# uploads the image to the cloud storage bucket
def upload_image(filename):
    bucket_name = 'college_basketball_bucket'
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(filename)
    blob.upload_from_filename(filename)
    return

# retrieves the image for the specified college and returns the url to the image
def retrieve_image(college_name):
    bucket_name = 'college_basketball_bucket'
    filename = f'images/{college_name}-past-record.jpg'
    storage_client = storage.Client()
    blob = storage_client.bucket(bucket_name).get_blob(filename)
    blob.make_public()
    return blob.public_url