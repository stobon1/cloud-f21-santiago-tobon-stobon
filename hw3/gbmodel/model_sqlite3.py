"""
A simple guestbook flask app.
Data is stored in a SQLite database that looks something like the following:

+------------------+---------------+---------+------+---------------+---------+
|    Department    | Course Number | Quarter | Year |  Instructor   | Review
+==================+===============+=========+------+---------------+---------+
| Computer Science |    430        |  Fall   | 2021 | Wu-chang Feng | great class! |
+------------------+---------------+---------+------+-------------------------+

This can be created with the following SQL (see bottom of this file):

    create table guestbook (department text, course_num text, quarter text, year int, instructor text, review text);

"""
from datetime import date
from .Model import Model
import sqlite3
DB_FILE = 'entries.db'    # file for our Database

class model(Model):
    def __init__(self):
        # Make sure our database exists
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        try:
            cursor.execute("select count(rowid) from guestbook")
        except sqlite3.OperationalError:
            cursor.execute("create table guestbook (department text, course_num text, quarter text, year int, instructor text, review text)")
        cursor.close()

    def select(self):
        """
        Gets all rows from the database
        Each row contains: department, course number, quarter, year, instructor, review
        :return: List of lists containing all rows of database
        """
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM guestbook")
        return cursor.fetchall()

    def insert(self, department, course_num, quarter, year, instructor, review):
        """
        Inserts entry into database
        :param department: String
        :param course_num: String
        :param quarter: String
        :param year: Int
        :param instructor: String
        :param review: String
        :return: True
        :raises: Database errors on connection and insertion
        """
        params = {'department':department, 'course_num':course_num, 'quarter':quarter, 'year':year, 'instructor':instructor, 'review':review}
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("insert into guestbook (department, course_num, quarter, year, instructor, review) VALUES (:department, :course_num, :quarter, :year, :instructor, :review)", params)

        connection.commit()
        cursor.close()
        return True
